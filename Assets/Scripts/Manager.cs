using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour
{

    public void LoadScene(int index){
        SceneManager.LoadScene(index);
    }

    public void Download () {
        StartCoroutine(DownloadFile());
    }

    IEnumerator DownloadFile() {
        var uwr = new UnityWebRequest("unity3d.com", UnityWebRequest.kHttpVerbGET);
        string path = Path.Combine(Application.persistentDataPath, "unity3d.html");
        uwr.downloadHandler = new DownloadHandlerFile(path);
        yield return uwr.SendWebRequest();
        if (uwr.result != UnityWebRequest.Result.Success)
            Debug.LogError(uwr.error);
        else
            Debug.Log("File successfully downloaded and saved to " + path);
    }

    public void OpenURLKirchoff(){
        Application.OpenURL("https://drive.google.com/drive/folders/1CLYpjw1lIIWoAdop2itThLbjSeRWy0R0?usp=sharing");
    }

    public void OpenURLOhm(){
        Application.OpenURL("https://mega.nz/file/Md80UJoC#hxqAOyY1A5RR911devD9nZ5EFhJ4LxzSOpWiFmVR8ys");
    }

    public void OpenURLRangkaian(){
        Application.OpenURL("https://mega.nz/file/8Z8B1Kpa#qzNqqcIWxZ2xuBt5UokY8GHvJnw5484AvifxjujeCAA");
    }

}
