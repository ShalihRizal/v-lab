using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Vid : MonoBehaviour
{
    private void Awake() {
        StartCoroutine("WaitForVid");
    }

    IEnumerator WaitForVid()
    {
        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene(1);
    }
}
