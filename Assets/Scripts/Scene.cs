using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scene : MonoBehaviour
{
    public void loadScene(int index){
        SceneManager.LoadScene(index);
    }

    public void Quit(){
        Application.Quit();
    }

}
