using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Saklar : MonoBehaviour
{

    public Sprite newImage;
    public Button button;

    public void ChangeImage(){
        button.image.sprite = newImage;
    }
}
